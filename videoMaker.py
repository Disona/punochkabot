import matplotlib
matplotlib.use('Agg')			# From stack overflow
import logging
import sys
import requests
import help_strings as st
import pathlib
import time
import moviepy.video.io.ImageSequenceClip as movpy
import os
from PIL import Image
from PIL import ImageFont
from PIL import ImageDraw
import ffmpeg
import tg_bot

class VideoMaker:
	def __init__(self):
		self.photoStoragePath = st.PHOTO_STORAGE					# Where to store photos
		self.videoStoragePath = st.VIDEO_STORAGE					# Where to store videos
		self.clipsStoragePath = st.CLIP_STORAGE						# Where to store videos
		self.last_update_id = 0 									# ID of last telegram update
		self.photoChan = st.PHOTOS_CHANNEL 							# Channel ID where to get photos from
		self.fps = 60 												# FPS of output video
		self.owmApiKey = 'af469d7e80546c84b89f8244d19db486' 		# API key for OpenWeatherMap
		self.owmUrl = 'https://api.openweathermap.org/data/2.5/' 	# Base URL for OWM
		self.fileNameFormat = '%y.%m.%d_%H.%M.%S' 					# Format for file name and time
		self.wLat = '55.602379' 									# Latitude of weather point
		self.wLon = '37.729517' 									# Magnitude of weather point
		self.minPhotos = 200										# Minimum photos to merge into clip
		self.audioClip = st.AUDIO_CLIP

		# Check if variables are set
		isOk = True
		if self.photoStoragePath == '':
			isOk = False
			logging.error("Photo storage dir is not set! Set it in help_string.py")

		if self.clipsStoragePath == '':
			isOk = False
			logging.error("Clips storage dir is not set! Set it in help_string.py")

		if self.videoStoragePath == '':
			isOk = False
			logging.error("Output video storage dir is not set! Set it in help_string.py")

		if isOk != True:
			logging.info("Some variables are not set. Exiting")
			exit(-1)

		# Check if there is a storage directory and if not, create one
		if not pathlib.Path(self.photoStoragePath).is_dir():
			logging.info("There was no photo storage dir. Creating new one.")
			pathlib.Path(self.photoStoragePath).mkdir()
		else:
			logging.info("Photo storage dir exists")

		# Check if there is a storage directory and if not, create one
		if not pathlib.Path(self.videoStoragePath).is_dir():
			logging.info("There was no output video storage dir. Creating new one.")
			pathlib.Path(self.videoStoragePath).mkdir()
		else:
			logging.info("Output video storage dir exists")

		# Check if there is a storage directory and if not, create one
		if not pathlib.Path(self.clipsStoragePath).is_dir():
			logging.info("There was no temporal clips storage dir. Creating new one.")
			pathlib.Path(self.clipsStoragePath).mkdir()
		else:
			logging.info("Clips storage dir exists")


		return



	# Checks if anyone asked bot for health
	def checkHealthRequest(self, updates):
		for update in updates:
			if 'message' not in update:
				continue
			if 'text' not in update['message']:
				continue
			if update['message']['text'].lower() != 'ping':
				continue

			logging.info('Got health request')

			# Collect all photos, clips and outputs
			imageList = []
			clipList = []
			outputList = []
			imageFiles = pathlib.Path(self.photoStoragePath).glob('*.jpg')
			clipFiles = pathlib.Path(self.clipsStoragePath).glob('*.mp4')
			outputFiles = pathlib.Path(self.videoStoragePath).glob('*.mp4')

			for file in imageFiles:
				imageList.append(str(file))

			for file in clipFiles:
				clipList.append(str(file))

			for file in outputFiles:
				outputList.append(str(file))

			# Sort that
			imageList.sort()
			clipList.sort()
			outputList.sort()

			# Prepare text
			gotAnything = False
			textString = 'Драсьте\nУ меня тут есть всякое.\n'
			if len(imageList) > 0:
				gotAnything = True
				textString += 'Фотки: ' + str(len(imageList)) + '\n'
				textString += 'Первая: ' + imageList[0].split(os.sep)[-1] + '\n'
				textString += 'Последняя: ' + imageList[-1].split(os.sep)[-1] + '\n'
				textString += '\n'
			else:
				textString += 'Только вот фоток нет.\n'

			if len(clipList) > 0:
				gotAnything = True
				textString += 'Клипы:\n'
				for clip in clipList:
					textString += clip.split(os.sep)[-1] + '\n'
			else:
				if gotAnything == False:
					textString += 'Клипов тоже нет.\n'
				else:
					textString += 'Клипов нет.\n'

			textString += '\n'

			if len(outputList) > 0:
				gotAnything = True
				textString += 'Видео:\n'
				for video in outputList:
					textString += video.split(os.sep)[-1] + '\n'
			else:
				if gotAnything == False:
					textString += 'Да и видео нет.\n'
				else:
					textString += 'А видео нет.\n'

			chatId = update['message']['chat']['id']
			tg_bot.tgb.send_msg(textString, chatId)


	def checkNewPhotos(self, updates):
		if updates == []:
			logging.debug('Now new updates')
			return None

		updCnt = len(updates)
		logging.debug('Got ' + str(updCnt) + ' update(s)')
		
		# Check all updates. Leave only "channel post" from target channel
		# and only with photos
		photoIDsArray = []
		for update in updates:
			if ('channel_post' in update):
				if update['channel_post']['sender_chat']['id'] == self.photoChan:
					logging.debug('Found post from target channel')
					if 'photo' in update['channel_post']:
						logging.debug('Found photo')
						photoArray = update['channel_post']['photo']
						timestamp = update['channel_post']['date'] + 3 * 60 * 60 # Add 3 hours for Moscow UTC
						# Now get the largest photo
						maxWidth = 0
						for photo in photoArray:
							if photo['width'] > maxWidth:
								largestPhotoId = photo['file_id']
								maxWidth = photo['width']

						# Now save files ID and timestamp in the array
						photoIDsArray.append({'id': largestPhotoId, 'ts' : timestamp})

		# Check if we got anything
		if photoIDsArray != []:
			logging.debug('Found ' + str(len(photoIDsArray)) + ' photos')
			return photoIDsArray
		else:
			return None

	# Gets link for each photo and download it to a file
	# File name is generated from photo timestamp in "YY.MM.DD_HH.MM.SS" format in local Moscow Time
	def downloadPhotos(self, photoArray, resize = True):
		logging.debug('Downloading files...')
		donwloadedCnt = 0
		total = len(photoArray)
		for photo in photoArray:
			photoId = photo['id']
			photoTs = photo['ts']
			t = time.gmtime(photoTs)							# Convert timestamp to time_struct for "strftime" with GM time, because timestamp is for Moscow time already
			fileName = time.strftime(self.fileNameFormat, t)	# Convert time_struct to string in desired format
			
			# Check if the file with such name exists. If so, add a number to the end of the name
			photoFile = pathlib.Path(self.photoStoragePath + fileName + '.jpg')
			if photoFile.is_file():
				copyNum = 1
				while photoFile.is_file():
					photoFile = pathlib.Path(self.photoStoragePath + fileName + '_' + str(copyNum) + '.jpg')
					copyNum = copyNum + 1

			# Get download link and download a file
			logging.debug('Getting link for file: ' + str(photoId) + '...')

			dnLink = tg_bot.tgb.get_file(photoId)
			if dnLink == -1:
				logging.warning('Could not get link for: ' + str(photoId))
				continue

			logging.debug('Got link: ' + str(dnLink))

			logging.debug('Downloading file... ' + str(donwloadedCnt + 1) + ' / ' + str(total))
			fileContent = tg_bot.tgb.download_file(dnLink)
			if fileContent == -1:
				logging.warning('Could not download file from: ' + dnLink)
				continue

			donwloadedCnt = donwloadedCnt + 1
			logging.debug('File downloaded.')

			# Save the file
			logging.debug('Saving file ' + str(photoFile))
			tempFile = open(photoFile, 'wb')
			tempFile.write(fileContent)
			tempFile.close()

			if resize:
				# Resize file to 1280x720
				img = Image.open(photoFile)
				imgW, imgH = img.size
				ratio = 1280.0 / imgW
				newSize = (int(imgW * ratio), int(imgH * ratio))
				newImg = img.resize(newSize, resample=0, box=None)
				img.close()
				newImg.save(photoFile)


		logging.info('Downloaded ' + str(donwloadedCnt) + ' files')
		return

	# Adds time (input as timestamp) and temperature to the photo
	def addTimeAndTempToPhotos(self, imageList, temperatureData):
		# Get the first photo to calculate text position
		# Open image and get size
		img = Image.open(imageList[0])
		imgW, imgH = img.size
		img.close()	

		# Now calc font sizes for the text.
		# Text sizes were tested for 1920x1080, but the photos are resized to 1280x720 when saved.
		# So we need to recalculate that a bit
		# For 1920x1080 image nice sizes are:
		# date - 100pt in Segue UI Light
		# time - 50pt in Consolas
		# temp - 50pt in Consolas
		# So take that as a proportions
		datePt = int(100.0 * imgW / 1920.0)
		timePt = int(50.0 * imgW / 1920.0)
		tempPt = int(50.0 * imgW / 1920.0)

		# Now prepare fonts
		dateF = ImageFont.truetype("segoeuil.ttf", datePt)
		timeF = ImageFont.truetype("consola.ttf", timePt)
		tempF = ImageFont.truetype("consola.ttf", tempPt)

		# Set coordinates of RIGHT top corner of Date text in % of original size
		dateX = int(imgW * 0.9)				# For date
		dateY = int(imgH * 0.1)

		# Get date string height to put time lower
		dateH = dateF.getsize('00.00.0000')[1]
		timeX = dateX						# For time
		timeY = dateY + dateH 				# For some reason there is no need in additional spacing after Segoe

		# Get time string height to put temperature lower
		timeH = timeF.getsize('00:00:00')[1]
		tempX = timeX						# For temperature
		tempY = timeY + timeH + 0.5 * timeH # But for consolas we need to add some vertical spacingtimeH

		i = 0
		frameCounter = 0
		printDots = 1 						# Flag to print dots in HH:MM string 			
		# Now cycle through photos
		for filePath in imageList:
			# Decode time from photo. It's first 17 characters
			fileName = filePath.split(os.sep)[-1]					# Get only name of the file
			fileDate = fileName[0:17]							# Get date from name
			t = time.strptime(fileDate, self.fileNameFormat)	# Convert into time_struct

			# If we have temperatures array - try to get current temperature
			currentTemperature = '---'
			if temperatureData:
				# Now find two temperatures - in the beggining of THIS hour and
				# in the beggining of THE NEXT hour. And both in this day.
				currentHour = t.tm_hour
				currentDay = t.tm_mday

				idx = -1

				for d in temperatureData:
					idx = idx + 1
					tempStruct = time.localtime(d['ts'])
					dHour = tempStruct.tm_hour
					dDay = tempStruct.tm_mday

					if dHour == currentHour and dDay == currentDay:
						break

				if temperatureData != None and idx < (len(temperatureData) - 1):
					startTemp = temperatureData[idx]['temp']
					endTemp = temperatureData[idx + 1]['temp']

					# Now we can interpolate temperature to current seconds inside the hour
					secondsInHour = t.tm_sec + t.tm_min * 60
					hourPercent = secondsInHour * (1.0 / 3600.0)
					currentTemperature = str(round(startTemp + hourPercent * (endTemp - startTemp), 1))
				else:
					currentTemperature = '---'

				# Debug stuff
				if i == 0 and idx < (len(temperatureData) - 1):
					logging.debug('Current file date: ' + fileDate + ' - hour: ' + str(currentHour))
					logging.debug('Temperature start date: ' + time.strftime('%Y/%m/%d, %H:%M:%S', time.localtime(temperatureData[idx]['ts'])))
					logging.debug('Temperature end date: ' + time.strftime('%Y/%m/%d, %H:%M:%S',  time.localtime(temperatureData[idx + 1]['ts'])))
				i = i + 1
				if i == 10:
					i = 0

			# Open image and get size
			img = Image.open(filePath)

			# Prepare data strings
			dateString = time.strftime('%d.%m.%Y', t)	# Convert time_struct to string in desired format
			timeString = time.strftime('%H:%M', t)	# Convert time_struct to string in desired format
			tempString = currentTemperature + ' C'

			# To draw or not to draw dots for "clock" effect
			frameCounter += 1
			if (frameCounter == 30):
				printDots ^= 1
				frameCounter = 0

			if printDots == 0:
				timeString = timeString[0:2] + ' ' + timeString[3:5]

			# Create drawer
			draw = ImageDraw.Draw(img)
			draw.text((dateX, dateY), dateString, (255,255,255), stroke_width = 2, stroke_fill = (0,0,0), font = dateF, anchor = 'rt')
			draw.text((timeX, timeY), timeString, (255,255,255), stroke_width = 2, stroke_fill = (0,0,0), font = timeF, anchor = 'rt')
			draw.text((tempX, tempY), tempString, (255,255,255), stroke_width = 2, stroke_fill = (0,0,0), font = tempF, anchor = 'rt')
			img.save(filePath)
			img.close()

		return

	# Creates a video from images in storage path
	def createShortClip(self, temperatureData, deleteFiles = False):
		logging.info("Creating short clip...")
		logging.debug('Getting list of files...')
		# Get image files list and convert it into list of strings
		imageFiles = pathlib.Path(self.photoStoragePath).glob('*.jpg')
		
		imageList = []
		for file in imageFiles:
			imageList.append(str(file))

		if imageList == []:
			logging.info('No images found')
			return
			
		# Sort by name
		imageList.sort()

		# Take the first and the last photos time for video filename
		firstFileName = imageList[0].split(os.sep)[-1][0:17]					# Get only name of the file
		lastFileName = imageList[-1].split(os.sep)[-1][0:17]					# Get only name of the file
		videoFileName = firstFileName + '___' + lastFileName + '.mp4'
		logging.info('Found ' + str(len(imageList)) + ' photos')

		# Now add some text to each photo
		logging.info('Adding text to images...')
		self.addTimeAndTempToPhotos(imageList, temperatureData)

		time.sleep(2)
		logging.info('Merging photos...')
		clip = movpy.ImageSequenceClip(imageList, fps = self.fps)
		clip.write_videofile(self.clipsStoragePath + videoFileName)
		clip.close()
		logging.info('Done creating short clip')
		# Now we can delete our JPG-files
		if deleteFiles:
			logging.info('Deleting ' + str(len(imageList)) + ' photos...')
			for file in imageList:
				try:
					pathlib.Path(file).unlink()
				except Exception as e:
					logging.warning('File ' + file + ' does not exist')

		return self.clipsStoragePath + videoFileName

	# Joins several short clips into long one using 'ffmpeg-python'
	def joinClipsWithFfmpeg(self, clipList, outputName = None):
		# Can run only if there is more that one clips
		if len(clipList) < 2:
			logging.warning('Only ' + str(len(clipList)) + ' clip for concatenating given. Stop joining.')
			return None

		# Check if name was given. If not - save as "outputClip_N.mp4"
		if outputName == None:
			outputName = 'outputClip'

		outputFile = pathlib.Path(self.videoStoragePath + outputName + '.mp4')

		# Check if the file exists. If yes - create new one adding "_N" at the end
		if outputFile.is_file():
			copyNum = 1
			while outputFile.is_file():
				outputFile = pathlib.Path(self.videoStoragePath + outputName + '_' + str(copyNum) + '.mp4')
				copyNum = copyNum + 1

		logging.info('Joining clips into \'' + str(outputFile) + '\'...')

		# Creating 'input' instances
		inputsList = []

		for clip in clipList:
			inputsList.append(ffmpeg.input(clip))

		joined = ffmpeg.concat(*tuple(inputsList))
		out = ffmpeg.output(joined, str(outputFile))
		out.run()

		# Return path to file
		return str(outputFile)

	# Collects all clips from Clips storage and creates full output video
	def createFullClip(self, deleteFiles = False):
		logging.info('Joining clips')

		# Get clip files list and convert it into list of strings
		clipFiles = pathlib.Path(self.clipsStoragePath).glob('*.mp4')
		clipList = []
		for clip in clipFiles:
			clipList.append(str(clip))

		# Check if there is more than 1 clip
		if len(clipList) < 2:
			logging.warning('Only ' + str(len(clipList)) + ' clip for concatenating given. Nothing to join.')
			return None

		# Sort list
		clipList.sort()

		# Create name for the output clip. For that take the first part of the first file name
		# and the second part of the last file name
		firstFileName = str(clipList[0])
		lastFileName = str(clipList[-1])
		firstFileName = firstFileName.split(os.sep)[-1]		# Kepp only name of the file
		lastFileName = lastFileName.split(os.sep)[-1]
		firstFileName = firstFileName[0:-4]					# Remove extension ".mp4" in simple way
		lastFileName = lastFileName[0:-4]
		firstFileName = firstFileName.split('___')[0]
		lastFileName = lastFileName.split('___')[-1]
		outputFileName = firstFileName + '___' + lastFileName

		outputFile = self.joinClipsWithFfmpeg(clipList, outputFileName)

		if outputFile != None:
			logging.info('Successfully joined clips for \'' + outputFileName + '\'')
			# Now we can delete our MP4-files
			if deleteFiles:
				logging.info('Deleting ' + str(len(clipList)) + ' clips...')
				for file in clipList:
					try:
						pathlib.Path(file).unlink()
					except Exception as e:
						logging.warning('File ' + file + ' does not exist')
			return outputFile
		else:
			logging.error('Could not join clips for \'' + outputFileName + '\'')
			return None

	def addMusic(self, videoClipPath):
		# Compare durations (in seconds) of audio and video and cut audio if needed. 
		# Or loop it.
		videoClip = VideoFileClip(videoClipPath)
		audioClip = AudioFileClip(self.audioClip)
		videoDuration = videoClip.duration
		audioDuration = audioClip.duration

		if (audioDuration > videoDuration):
			audioClip = audioClip.subclip(0, videoDuration)
		else:
			audioClip = afx.audio_loop(audioClip, duration = videoDuration)

		# Add 1 second fade in and 6 seconds fadeout
		audioClip = audioClip.audio_fadein(1)
		audioClip = audioClip.audio_fadeout(5)
		videoClip = videoClip.set_audio(audioClip)

		# Add '.mp4' to output file, because moviepy had some problems
		# writing into the same file
		videoClipPathWA = videoClipPath + '.mp4'
		videoClip.write_videofile(videoClipPathWA)
		audioClip.close()
		videoClip.close()

		# Now delete clip without audio
		try:
			pathlib.Path(videoClipPath).unlink()
		except Exception as e:
			logging.warning('File ' + videoClipPath + ' does not exist')
		return videoClipPathWA

	# Sends latest log file
	def checkLogRequest(self, updates):
		for update in updates:
			if 'message' not in update:
				continue
			if 'text' not in update['message']:
				continue
			if update['message']['text'] != 'logs':
				continue

			logging.info('Got logs request')
			logFiles = pathlib.Path(os.getcwd()).glob('*_log.txt')

			logList = []
			for file in logFiles:
				logList.append(str(file))
			
			logList.sort()
			
			if len(logList) != 0:
				lastLog = logList[-1]
				logging.info('Sending log: ' + lastLog)
				chatId = update['message']['chat']['id']
				tg_bot.tgb.send_file(lastLog, 'document', chatId)
				return 0

	# Searches "stopbot" in requests
	def checkStopRequest(self, updates):
		for update in updates:
			if 'message' not in update:
				continue
			if 'text' not in update['message']:
				continue
			if update['message']['text'] != 'stopbot':
				continue
			else:
				return True

		return False

vm = VideoMaker()