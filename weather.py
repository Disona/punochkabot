import logging
import sys
import requests
import help_strings as st
import pathlib
import time
import os

class Weather:
	def __init__(self):
		self.owmApiKey = 'af469d7e80546c84b89f8244d19db486' 		# API key for OpenWeatherMap
		self.owmUrl = 'https://api.openweathermap.org/data/2.5/' 	# Base URL for OWM
		self.wLat = '55.602379' 									# Latitude of weather point
		self.wLon = '37.729517' 									# Magnitude of weather point


	# Gets temperature from the OWM.org
	# Returns temperature as A STRING. If not successfull - returns -1 (as int)
	def getCurrentTemperature(self):
		logging.info('Requesting current temperature...')
		target_url = (self.owmUrl +
			'weather' +
			'?lat=' + self.wLat +
			'&lon=' + self.wLon +
			'&units=' + 'metric' +
			'&APPID=' + self.owmApiKey)

		# Try to request temperature
		try:
			resp = requests.post(target_url, timeout=5)
		except Exception as e:
			logging.error('Error getting current temperature. Exception raised:' + str(e))
			return None

		if resp.status_code != 200:
			logging.error('Error getting current temperature. Request status code: ' + str(resp.status_code))
			return None

		# Parse data and convert temp from Kalvin to Celcius
		data = resp.json()
		return  round(data['main']['temp'], 1)

	# Returns list of dictionaries {'ts':<timestamp>, 'temp':<temperature>} for requested date
	# dateTs is timestamp of needed date
	def getHistoricalTemperature(self, dateTs):
		# Make URL
		# Dont forget to substract 3 (3600 secs * 3) hours (because of timezones)
		target_url = (self.owmUrl +
			'onecall/timemachine' +
			'?lat=' + self.wLat +
			'&lon=' + self.wLon +
			'&dt=' + str(dateTs) +
			'&units=' + 'metric' +
			'&APPID=' + self.owmApiKey)

		logging.info('Requesting temperature for date: ' + str(dateTs-3600*3) + ' (' + time.ctime(dateTs-3600*3) + ')')

		# Try to request temperature
		try:
			resp = requests.post(target_url, timeout=5)
		except Exception as e:
			logging.error('Error getting historical temperature. Exception raised:' + str(e))
			return -1

		if resp.status_code != 200:
			logging.error('Error getting historical temperature. Request status code: ' + str(resp.status_code))
			return -1

		# Save needed data as a list of dictionarires
		# Temperature is rounded to 1 digit after point
		output = []
		data = resp.json()
		for d in data['hourly']:
			output.append({'ts':(d['dt']), 'temp':round(d['temp'], 1)})
		return output

	# Gets hourly temperature for last several hours AND current temperature
	# https://api.openweathermap.org/data/2.5/onecall/timemachine?lat=55.602379&lon=37.729517&dt=1640597629&appid=af469d7e80546c84b89f8244d19db486 for history
	def getSomeHoursTemperature(self, periodHours):
		logging.debug('Requesting temperature...')
		# If someone wants more than 25 hours - fuck him
		if (periodHours > 25):
			periodHours = 25

		# Take timestamp of today and yesterday
		todayTs = int(time.time())
		yesterdayTs = todayTs - 86400 			# 60 * 60 * 24 = 86400 seconds in a day

		# Prepare list to store data
		tempData = []
		
		ret = self.getHistoricalTemperature(yesterdayTs)
		if ret != -1:
			tempData = ret
		else:
			logging.warning('Could not request yesterdays temperature')
			return None

		ret = self.getHistoricalTemperature(todayTs)
		if ret != -1:
			tempData = tempData + ret
		else:
			logging.warning('Could not request todays temperature')
			return None

		# Now we have data for 48 hours (at most). Keep only needed amount of hours from this array
		# It's a way to escape UTC 3-hours shift if we get time before 03:00:00
		tempData = tempData[-periodHours:]

		# Now get current temperature and add it in the end
		nowTemp = self.getCurrentTemperature()
		nowTime = time.time()
		if nowTemp:
			tempData.append({'ts':nowTime, 'temp':nowTemp})

		for tD in tempData:
			logging.info('Got Temp: ' + time.ctime(tD['ts']) + ' / ' + str(tD['temp']))

		return tempData

weather = Weather()