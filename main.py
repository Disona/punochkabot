#!/usr/bin/env python3
import logging
import sys
import requests
import help_strings as st
import pathlib
import time
import tg_bot
import videoMaker
import weather
#-----------------------------------------------------
# Functions to setup logging module

#-----------------------------------------------------

# This one setups logger. Must be called before anything else in the script
def setupLogging(toTerminal, toFile, level = logging.INFO):
	log = logging.getLogger()  # root logger - Good to get it only once.
	formatter = logging.Formatter('%(asctime)s [%(funcName)s] %(levelname)s: %(message)s')

	# If we want to log into a file - add file handler
	if toFile == True:
		logFileName = time.strftime("%Y.%m.%d",time.localtime()) + '_log.txt'
		filehandler = logging.FileHandler(logFileName, 'a')
		filehandler.setFormatter(formatter)
		log.addHandler(filehandler)      # set the new handler

	# If we want to log into console - add stream handler with stdout
	if toTerminal == True:
		streamhandler = logging.StreamHandler(sys.stdout)
		streamhandler.setFormatter(formatter)
		log.addHandler(streamhandler)      # set the new handler
	
	# Set logs level. By default the level is "INFO"
	log.setLevel(level)

# This one rotates log file. Should be called when the date changes
def rotateLogFile():
	logFileName = time.strftime("%Y.%m.%d",time.localtime()) + '_log.txt'
	log = logging.getLogger()  # root logger - Good to get it only once.
	filehandler = logging.FileHandler(logFileName, 'a')
	formatter = logging.Formatter('%(asctime)s [%(funcName)s] %(levelname)s: %(message)s')
	filehandler.setFormatter(formatter)

	for hdlr in log.handlers[:]:  	# remove the existing file handlers
		if isinstance(hdlr,logging.FileHandler):
			log.removeHandler(hdlr)

	log.addHandler(filehandler)     # set the new handler
	

		
# Setup logging
setupLogging(toTerminal = False, toFile = True, level = logging.INFO)

totalPhotos = 0

# To catch the day chang
nowDay = time.localtime(time.time()).tm_mday
prevDay = nowDay

tg_bot.tgb.send_msg('Меня запустили. Тюлюлюлю.')

while True:
	# Check new photos
	logging.debug('Checking updates...')

	# Get Updates
	updates = tg_bot.tgb.get_updates(tg_bot.tgb.token)

	# Check if we were asked to stop
	if videoMaker.vm.checkStopRequest(updates):
		tg_bot.tgb.send_msg('Датвиданиня')
		# Before extiting get updates one more time to inform TG that we have fetched the latest update
		# Otherwise the bot will always exit after restart, because it will get "Stop" command every time
		# We will lose some photos, but what can we do...
		updates = tg_bot.tgb.get_updates(tg_bot.tgb.token)
		exit(0)

	videoMaker.vm.checkHealthRequest(updates)
	videoMaker.vm.checkLogRequest(updates)
	photos = videoMaker.vm.checkNewPhotos(updates)


	# If we got something - download
	if photos:
		totalPhotos = totalPhotos + len(photos)
		videoMaker.vm.downloadPhotos(photos)

	# Create small clip
	if totalPhotos > videoMaker.vm.minPhotos:
		tempData = weather.weather.getSomeHoursTemperature(25)		# Get last 25 hours temperature data
		videoFile = videoMaker.vm.createShortClip(tempData, True)
		totalPhotos = 0
	
	# If the day changed - create long clip
	nowDay = time.localtime(time.time()).tm_mday
	if nowDay != prevDay:
		rotateLogFile()
	# Check if some photos are left
		if totalPhotos != 0:
			tempData = weather.weather.getSomeHoursTemperature(25)		# Get last 25 hours temperature data
			videoFile = videoMaker.vm.createShortClip(tempData, True)
			totalPhotos = 0
		prevDay = nowDay
		output = videoMaker.vm.createFullClip(True)
		#output = vm.addMusic(output)
		if output != None:
			tg_bot.tgb.send_file(output, 'video')
		else:
			logging.warning("Couldn't create video")

	time.sleep(15)

exit(0)