#!/usr/bin/env python3
import logging
import sys
import requests
import help_strings as st
import pathlib
import time
import os


class tgBot:
	def __init__(self):
		self.token = st.BOT_TOKEN
		self.tg_url = st.TG_API_URL
		self.parse_mode = st.PARSE_MODE
		self.chat_id = st.TEST_CHAN
		self.last_update_id = 0
		self.ping_chat = 0

	def make_italic(self, text):
		return '<i>' + text + '</i>'

	def make_bold(self, text):
		return '<b>' + text + '</b>'
	
	def make_code(self, text):
		return '<code>' + text + '</code>'

	def inline_link(self, url, text):
		'''
		Создаёт кликабельную ссылку в телеге
		'''
		inlined = "<a href=\""
		inlined += url
		inlined += "\">"
		inlined += text
		inlined += "</a>"
		return inlined
	
	def send_msg(self, text, chatId = 0):
		if chatId == 0:
			chatId = str(self.chat_id)
		else:
			chatId = str(chatId)

		target_url = (self.tg_url +
						'bot' + self.token +
						'/sendMessage?' +
						'chat_id=' + chatId + '&' +
						'parse_mode=' + self.parse_mode + '&' +
						'text=' + requests.utils.quote(text))
		try:
			resp = requests.post(target_url, timeout=5)
		except Exception as e:
			return -1

		return resp.status_code

	# Get an update and return it as a dictionary
	def get_updates(self, bot_token = 0, offset = None):
		# If no token provided - use default bot
		if bot_token == 0:
			logging.info('Using default bot token: ' + str(self.token))
			bot_token = self.token

		# If no offset provided - set it as last known update
		if offset == None:
			logging.debug('Update offdset set to: ' + str(self.last_update_id))
			offset = self.last_update_id

		# If non-zero offset provided - get updates from that offset
		if offset != 0:
			target_url = (self.tg_url +
						'bot' + bot_token +
						'/getUpdates' + '?' +
						'offset=' + str(offset + 1))
		else:
			target_url = (self.tg_url +
						'bot' + bot_token +
						'/getUpdates')

		# Try to get updates
		try:
			resp = requests.post(target_url, timeout=5)
		except Exception as e:
			logging.error('Error sending request')
			return ''

		# Parse data
		data = resp.json()

		# Save last update ID (if there were any)
		if data['result'] != []:
			self.last_update_id = data['result'][-1]['update_id']
			logging.debug('Last update ID: ' + str(self.last_update_id))
		
		return data['result']

	# Returns a link to a file. If not successful - returns -1
	def get_file(self, file_id):
		target_url = (self.tg_url +
			'bot' + self.token +
			'/getFile' + '?' +
			'file_id=' + str(file_id))

		try:
			resp = requests.post(target_url, timeout=5)
		except Exception as e:
			return -1

		if resp.status_code == 200:
			data = resp.json()
			return data['result']['file_path']
		else:
			return -1


	# Downloads file with a link and returns file content
	# In case of exception or if the file was not found returns '-1'
	def download_file(self, file_path):
		target_url = (self.tg_url +
			'file/' +
			'bot' + self.token +
			'/' + str(file_path))

		try:
			resp = requests.get(target_url, timeout=5)
		except Exception as e:
			return -1

		if resp.status_code == 200:
			return resp.content
		else:
			return -1

	# Sends a file
	def send_file(self, file, ftype, chat_id = None):
		logging.info('Request to send ' + ftype + ' file: ' + file)
		typesList = ['photo', 'document', 'audio', 'video', 'animation', 'voice', 'videonote']
		if ftype.lower() not in typesList:
			logging.error('Tried to send unknown file type:' + ftype)
			return -1

		if chat_id == None:
			chat_id = self.chat_id

		ftypeJson = ftype.lower()
		urlMethod = '/send' + ftype.lower().title()

		files = {ftypeJson: open(file, 'rb')}

		target_url = (self.tg_url +
			'bot' + self.token +
			urlMethod + '?' +
			'chat_id=' + str(chat_id))

		try:
			resp = requests.post(target_url, files = files, timeout = 5)
		except Exception as e:
			logging.error('Got exception while sending file:' + str(e))
			return -1

		if resp.status_code == 200:
			return 0
		else:
			logging.error('Got bad response:' + str(resp.status_code))
			return -1

tgb = tgBot()


